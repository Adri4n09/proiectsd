package com.springapp.mvc.controller;

import com.springapp.mvc.model.Order;
import com.springapp.mvc.service.OrderService;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;


@RestController
@RequestMapping("/order/")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/orders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Order>> listOrders() {
        List<Order> orders = orderService.getAllOrders();
        if (orders.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> getOrder(@PathVariable("id") long idOrder) {
        Order order = orderService.getOrderById(idOrder);
        if(order == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addOrder(@RequestBody Order order, UriComponentsBuilder ucBuilder) {
        if (orderService.ifOrderExist(order)) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        orderService.addOrder(order);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/order/{id}").buildAndExpand(order.getIdOrder()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Order> updateOrder(@RequestBody long id, Order order) {
        Order currentOrder = orderService.getOrderById(id);

        if (currentOrder == null) {
           return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        currentOrder.setIdUser(order.getIdUser());
        currentOrder.setIsbn(order.getIsbn());
        currentOrder.setQuantity(order.getQuantity());
        currentOrder.setTotalPrice(order.getTotalPrice());

        orderService.updateOrder(currentOrder);
        return new ResponseEntity<>(currentOrder, HttpStatus.OK);
    }

    @RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Order> deleteOrder(@PathVariable("id") long id) {
        Order order = orderService.getOrderById(id);

        if (order == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        orderService.deleteOrder(order);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
