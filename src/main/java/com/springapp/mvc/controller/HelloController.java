package com.springapp.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class HelloController {
	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		return "editUser";
	}

	@RequestMapping(value="booksPage", method = RequestMethod.POST)
	public String getBooksPage(ModelMap model) {
		return "books";
	}

	@RequestMapping(value = "userPage", method = RequestMethod.GET)
	public String getUserPage(ModelMap model) {
		return "hello";
	}


	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String getIndex(ModelMap map) {
		return "index";
	}

	@RequestMapping(value="editBook", method = RequestMethod.GET)
	public String getEditBook(ModelMap map) {
		return "editBook";
	}

	@RequestMapping(value="editUser", method = RequestMethod.GET)
	public String getEditUser(ModelMap map) { return "editUser"; }

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String getLogin(ModelMap model) { return "login"; }
}