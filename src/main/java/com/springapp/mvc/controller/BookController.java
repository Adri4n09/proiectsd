package com.springapp.mvc.controller;

import com.springapp.mvc.model.Book;
import com.springapp.mvc.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.w3c.dom.html.HTMLParagraphElement;

import javax.jws.soap.SOAPBinding;
import java.util.List;

/**
 * Created by adri4n on 07.01.2016.
 */
@RestController
@RequestMapping("/book/")
public class BookController {

    @Autowired
    private BookService bookService;

    @RequestMapping(value = "/books/", method = RequestMethod.GET)
    public ResponseEntity<List<Book>> listAllBooks() {
        List<Book> books = bookService.getAllBooks();

        if(books.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @RequestMapping(value = "/book/{isbn}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> getBook(@PathVariable("isbn") String isbn) {
        Book book = bookService.getBookByIsbn(isbn);
        if(book == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(book, HttpStatus.OK);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Void> createBook(@RequestBody Book book, UriComponentsBuilder ucBuilder) {

        if(bookService.ifBookExist(book)) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        bookService.addBook(book);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/book/{id}").buildAndExpand(book.getIsbn()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/{isbn}", method = RequestMethod.PUT)
    public ResponseEntity<Book> updateBook(@PathVariable("isbn") String isbn, @RequestBody Book book) {
        Book currentBook = bookService.getBookByIsbn(isbn);

        if (currentBook==null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        currentBook.setAuthor(book.getAuthor());
        currentBook.setGenre(book.getGenre());
        currentBook.setPrice(book.getPrice());
        currentBook.setQuantity(book.getQuantity());

        bookService.updateBook(currentBook);
        return new ResponseEntity<>(currentBook, HttpStatus.OK);
    }

    @RequestMapping(value = "/book/{isbn}", method = RequestMethod.DELETE)
    public ResponseEntity<Book> deleteBook(@PathVariable("isbn") String isbn) {
        Book book = bookService.getBookByIsbn(isbn);

        if(book == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        bookService.deleteBook(book);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
