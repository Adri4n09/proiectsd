package com.springapp.mvc.dao;

import com.springapp.mvc.model.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by adri4n on 06.01.2016.
 */
@Transactional
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public User getUserById(long idUser) {
        List<User> list = getSessionFactory().getCurrentSession().createQuery("from User where idUser = ?")
                .setParameter(0, idUser).list();
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getAllUsers() {
        return (List<User>) getSessionFactory().getCurrentSession().createQuery("from User").list();
    }

    @Override
    public void addUser(User user) {
        getSessionFactory().getCurrentSession().save(user);
    }

    @Override
    public void deleteUser(User user) {
        getSessionFactory().getCurrentSession().delete(user);
    }

    @Override
    public void updateUser(User user) {
        getSessionFactory().getCurrentSession().update(user);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
