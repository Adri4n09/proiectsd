package com.springapp.mvc.dao;

import com.springapp.mvc.model.Book;

import java.util.List;

/**
 * Created by adri4n on 06.01.2016.
 */
public interface BookDao {

    Book getBookByISBN(String isbn);

    List<Book> getAllBooks();

    void addBook(Book book);

    void deleteBook(Book book);

    void updateBook(Book book);
}
