package com.springapp.mvc.dao;

import com.springapp.mvc.model.Book;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by adri4n on 06.01.2016.
 */
@Transactional
public class BookDaoImpl implements BookDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public Book getBookByISBN(String isbn) {
        List<Book> list = sessionFactory.getCurrentSession().createQuery("from Book where isbn = ?")
                .setParameter(0, isbn).list();

        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Book> getAllBooks() {
        List<Book> list = sessionFactory.getCurrentSession().createQuery("from Book").list();
        return list;
    }

    @Override
    public void addBook(Book book) {
        sessionFactory.getCurrentSession().save(book);
    }

    @Override
    public void deleteBook(Book book) {
        sessionFactory.getCurrentSession().delete(book);
    }

    @Override
    public void updateBook(Book book) {
        sessionFactory.getCurrentSession().update(book);
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}
