package com.springapp.mvc.dao;

import com.springapp.mvc.model.Order;

import java.util.List;

/**
 * Created by adri4n on 18.12.2016.
 */
public interface OrderDao {

    Order getOrderById(long idOrder);

    List<Order> getAllOrders();

    void addOrder(Order order);

    void deleteOrder(Order order);

    void updateOrder(Order order);
}
