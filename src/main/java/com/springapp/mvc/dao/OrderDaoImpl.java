package com.springapp.mvc.dao;

import com.springapp.mvc.model.Order;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public class OrderDaoImpl implements OrderDao{

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public Order getOrderById(long idOrder) {
        List<Order> list = getSessionFactory().getCurrentSession().createQuery("from Order where idOrder = ?")
                .setParameter(0, idOrder).list();
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Order> getAllOrders() {
        return getSessionFactory().getCurrentSession().createQuery("from Order").list();
    }

    @Override
    public void addOrder(Order order) {
        getSessionFactory().getCurrentSession().save(order);
    }

    @Override
    public void deleteOrder(Order order) {
        getSessionFactory().getCurrentSession().delete(order);
    }

    @Override
    public void updateOrder(Order order) {
        getSessionFactory().getCurrentSession().update(order);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
