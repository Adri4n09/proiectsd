package com.springapp.mvc.model;

/**
 * Created by adri4n on 06.01.2016.
 */
public class User {

    private long idUser;
    private String username;
    private String password;
    private String email;
    private String address;

    public User() {
    }

    public User(long idUser, String email, String password, String username, String address) {
        this.idUser = idUser;
        this.email = email;
        this.password = password;
        this.username = username;
        this.address = address;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
