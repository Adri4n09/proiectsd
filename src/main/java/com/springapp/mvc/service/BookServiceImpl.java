package com.springapp.mvc.service;

import com.springapp.mvc.dao.BookDao;
import com.springapp.mvc.model.Book;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by adri4n on 06.01.2016.
 */
public class BookServiceImpl implements BookService {

    @Autowired
    private BookDao bookDao;

    @Override
    public Book getBookByIsbn(String isbn) {
        return bookDao.getBookByISBN(isbn);
    }

    @Override
    public void addBook(Book book) {
        bookDao.addBook(book);
    }

    @Override
    public void deleteBook(Book book) {
        bookDao.deleteBook(book);
    }

    @Override
    public void updateBook(Book book) {
        bookDao.updateBook(book);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookDao.getAllBooks();
    }

    @Override
    public boolean ifBookExist(Book book) {
        return getBookByIsbn(book.getIsbn()) != null;
    }

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }
}
