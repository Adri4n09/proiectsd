package com.springapp.mvc.service;

import com.springapp.mvc.model.User;

import java.util.List;

/**
 * Created by adri4n on 06.01.2016.
 */
public interface UserService {

    User getUserById(long idUser);

    void addUser(User user);

    void deleteUser(User user);

    void updateUser(User user);

    List<User> getAllUsers();

    boolean ifUserExist(User user);
}
