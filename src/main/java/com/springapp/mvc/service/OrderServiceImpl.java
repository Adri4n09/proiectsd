package com.springapp.mvc.service;

import com.springapp.mvc.dao.OrderDao;
import com.springapp.mvc.model.Order;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by adri4n on 18.12.2016.
 */
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public Order getOrderById(long idOrder) {
        return orderDao.getOrderById(idOrder);
    }

    @Override
    public void addOrder(Order order) {
        orderDao.addOrder(order);
    }

    @Override
    public void deleteOrder(Order order) {
        orderDao.deleteOrder(order);
    }

    @Override
    public void updateOrder(Order order) {
        orderDao.updateOrder(order);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderDao.getAllOrders();
    }

    @Override
    public boolean ifOrderExist(Order order) {
        return getOrderById(order.getIdOrder()) != null;
    }

    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }
}
