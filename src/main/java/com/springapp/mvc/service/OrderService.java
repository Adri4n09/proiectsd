package com.springapp.mvc.service;

import com.springapp.mvc.model.Order;

import java.util.List;

/**
 * Created by adri4n on 18.12.2016.
 */
public interface OrderService {

    Order getOrderById(long idOrder);

    void addOrder(Order order);

    void deleteOrder(Order order);

    void updateOrder(Order order);

    List<Order> getAllOrders();

    boolean ifOrderExist(Order order);
}
