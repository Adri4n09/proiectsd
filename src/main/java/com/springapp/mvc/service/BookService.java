package com.springapp.mvc.service;

import com.springapp.mvc.model.Book;

import java.util.List;

/**
 * Created by adri4n on 06.01.2016.
 */
public interface BookService {

    Book getBookByIsbn(String isbn);

    void addBook(Book book);

    void deleteBook(Book book);

    void updateBook(Book book);

    List<Book> getAllBooks();

    boolean ifBookExist(Book book);
}
