/**
 * Created by adri4n on 11.02.2016.
 */

'use strict';

App.factory('BookService', ['$http','$q', function($http, $q){

    return {

        fetchAllBooks: function() {
            return $http.get('http://localhost:8081/book/books/')
                .then(
                function(response){
                    console.log('book: '+response.data);
                    return response.data;
                },
                function(errReponse){
                    console.error('Error while fetching books');
                    return $q.reject(errReponse);
                }
            );
        },

        createBook: function(book){
            return $http.post('http://localhost:8081/book/add',book)
                .then(
                function(response){
                    return response.data;
                },
                function (errResponse){
                    console.error('Error while creating book');
                    return $q.reject(errResponse);
                }
            );
        },

        updateBook: function(book, isbn) {
            return $http.put('http://localhost:8081/book/update/'+isbn, book)
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while updating book');
                    return $q.reject(errResponse);
                }
            );
        },

        deleteBook: function(isbn) {
            return $http.delete('http://localhost:8081/book/book/'+isbn)
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while deleteing book');
                    return $q.reject(errResponse);
                }
            );
        }

    };

}]);
