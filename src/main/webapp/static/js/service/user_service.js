/**
 * Created by adri4n on 09.02.2016.
 */

'use strict';
App.factory('UserService', ['$http', '$q', function($http, $q){

    return {

        fetchAllUsers: function() {
            return $http.get('http://localhost:8081/user/users/')
                .then(
                function(response){
                    console.log('users: ' + response.data);
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while fetchin users');
                    return $q.reject(errResponse);
                }
            );
        },

        createUser: function(user) {
            return $http.post('http://localhost:8081/user/add',user)
                .then(
                function (response){
                    console.log('create user service: ' + response.data);
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while creating user');
                    return $q.reject(errResponse);
                }
            );
        },

        updateUser: function(user, id){
            return $http.put('http://localhost:8081/user/update/'+id,user)
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while updating user');
                    return $q.reject(errResponse);
                }
            );
        },

        deleteUser: function(id) {
            return $http.delete('http://localhost:8081/user/delete/',+id)
                .then(
                function(response){
                    return response.data;
                },
                function(errResponse){
                    console.error('Error while deleteing user');
                    return $q.reject(errResponse);
                }
            );
        }
    };

}]);