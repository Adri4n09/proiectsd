/**
 * Created by adri4n on 11.02.2016.
 */

'use strict';

App.controller('BookController', ['$scope', 'BookService', function($scope, BookService){
    var self = this;
    self.book = {isbn:'',title:'',author:'',genre:'',quantity:'',price:''};
    self.books=[];

    self.fetchAllBooks = function() {
        BookService.fetchAllBooks()
            .then(
            function(d) {
                self.books = d;
            },
            function(errResponse){
                console.error('Error while fetching book');
            }
        );
    };

    self.createBook = function(book) {
        BookService.createBook(book)
            .then(
            self.fetchAllBooks,
            function(errResponse){
                console.error('Error while creating Book.');
            }
        );
    };

    self.updateBook = function(book, isbn) {
        BookService.updateBook(book, isbn)
            .then(
            self.fetchAllBooks,
            function(errResponse){
                console.error('Error while updating Book.');
            }
        );
    };

    self.deleteBook = function(isbn) {
        BookService.deleteBook(isbn)
            .then(
            self.fetchAllBooks,
            function(errResponse){
                console.error('Error while deleting Book.');
            }
        );
    };

    self.fetchAllBooks();

    self.submit = function() {
        if(self.book.isbn) {
            console.log('Saving new Book', self.book);
            self.createBook(self.book);
        } else {
            self.updateBook(self.book, self.book.isbn);
            console.log('Book update with isbn ', self.book.isbn);
        }
        self.reset();
    };

    self.edit = function(isbn){
        console.log('isbn to be edited', isbn);
        for(var i = 0; i < self.books.length; i++) {
            if (self.books[i].isbn === isbn) {
                self.book = angular.copy(self.books[i]);
                break;
            }
        }
    };

    self.remove = function(isbn) {
        console.log('isbn to be deleted ', isbn);
        if (self.book.isbn === isbn) {
            self.reset();
        }
        self.deleteBook(isbn);
    };
}]);