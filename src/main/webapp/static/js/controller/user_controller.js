/**
 * Created by adri4n on 09.02.2016.
 */
'use strict';

App.controller('UserController', ['$scope', 'UserService', function($scope, UserService){
    var self = this;
    self.user={idUser:'',username:'',password:'',email:'',address:''};
    self.users=[];

    self.fetchAllUsers = function(){
        UserService.fetchAllUsers()
            .then(
            function(d) {
                self.users = d;
            },
            function(errResponse) {
                console.error('Error while fetching Users.');
            }
        );
    };

    self.createUser = function(user) {
        UserService.createUser(user)
            .then(
            self.fetchAllUsers,
            function(errResponse) {
                console.error('Error while creating User.');
            }
        );
    };

    self.deleteUser = function(id) {
        UserService.deleteUser(id)
            .then(
            self.fetchAllUsers,
            function(errResponse){
                console.error('Error deleteing User.');
            }
        );
    };

    self.fetchAllUsers();

    self.submit = function() {
        if(self.user.idUser) {
            console.log('id to be edited', self.user.id);
            self.createUser(self.user);
        } else{
            self.updateUser(self.user, self.user.id);
            console.log('User update with id ', self.user.idUser);
        } self.reset();
    };

    self.updateUser = function(user, id) {
        UserService.updateUser(user, id)
            .then(
                self.fetchAllUsers,
                function(errResponse){
                    console.error('error while updating user.');
                }
            )
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.users.length; i++) {
            if(self.users[i].idUser === id) {
                self.user = angular.copy(self.users[i]);
                break;
            }
        }
    };

    self.remove = function(id) {
        console.log('id to be deleted', id);
        if (self.user.id === id) {
            self.reset();
        }
        self.deleteUser(id);
    };

    self.reset = function() {
        self.user = {idUser:null, username:'', password:'', email:'', address:''};
        $scope.myForm.$setPristine()
    };

}])