<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: adri4n
  Date: 05.12.2016
  Time: 21:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body ng-app="myApp">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/static/js/app.js' />"></script>
<script src="<c:url value='/static/js/service/user_service.js'/>"></script>
<script src="<c:url value='/static/js/controller/user_controller.js'/>"></script>
<div class="generic-container" ng-controller="UserController as ctrl">
    <form ng-submit="ctrl.submit()" name="myForm">
        <div class="form-group">
            <h2 class="col-md-offset-6">Edit User</h2>
        </div>
        <div class="form-group">
            <label>idUser:</label>
            <input id="idUser" class="form-control" type="text"
                   ng-model="ctrl.user.idUser" placeholder="IdUser"/>
        </div>
        <div class="form-group">
            <label>Username:</label>
            <input id="username" class="form-control" type="text"
                   ng-model="ctrl.user.username" placeholder="username"/>
        </div>
        <div class="form-group">
            <label>Password:</label>
            <input id="password" class="form-control" type="text"
                   ng-model="ctrl.user.password" placeholder="password"/>
        </div>
        <div class="form-group">
            <label>Email:</label>
            <input id="email" class="form-control" type="text"
                   ng-model="ctrl.user.email" placeholder="email"/>
        </div>
        <div class="form-group">
            <label>Address:</label>
            <input id="address" class="form-control" type="text"
                   ng-model="ctrl.user.address" placeholder="address"/>
        </div>
        <div class="form-group">
            <input class="btn btn-default" type="submit" value="Submit"/>
        </div>
    </form>
</div>

</body>
</html>
