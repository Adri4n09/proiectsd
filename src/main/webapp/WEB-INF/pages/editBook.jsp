<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: adri4n
  Date: 02.12.2016
  Time: 13:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body ng-app="myApp">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/static/js/app.js' />"></script>
<script src="<c:url value='/static/js/service/book_service.js'/>"></script>
<script src="<c:url value='/static/js/controller/book_controller.js'/>"></script>
<div class="generic-container" ng-controller="BookController as ctrl">
    <form ng-submit="ctrl.submit()" name="myForm">
        <div class="form-group">
            <h2 class="col-md-offset-6">Add book</h2>
        </div>
        <div class="form-group">
            <label>Isbn:</label>
            <input id="isbn" class="form-control" type="text"
                   ng-model="ctrl.book.isbn" placeholder="Isbn"></input>
        </div>
        <div class="form-group">
            <label>Title:</label>
            <input id="title" class="form-control" type="text"
                   ng-model="ctrl.book.title" placeholder="Title"></input>
        </div>
        <div class="form-group">
            <label>Author:</label>
            <input id="author" class="form-control" type="text"
                   ng-model="ctrl.book.author" placeholder="Author"></input>
        </div>
        <div class="form-group">
            <label>Genre:</label>
            <input id="gener" class="form-control" type="text"
                   ng-model="ctrl.book.genre" placeholder="Genre"></input>
        </div>
        <div class="form-group">
            <label>Quantity:</label>
            <input id="quantity" class="form-control" type="text"
                   ng-model="ctrl.book.quantity" placeholder="Quantity"></input>
        </div>
        <div class="form-group">
            <label>Price:</label>
            <input id="price" class="form-control" type="text"
                   ng-model="ctrl.book.price" placeholder="Price"></input>
        </div>
        <div class="form-group">
            <input class="btn btn-default" type="submit" value="Submit"/>
        </div>
    </form>
</div>
</body>
</html>
