<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
</head>
<body ng-app="myApp">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/static/js/app.js' />"></script>
<script src="<c:url value='/static/js/service/book_service.js'/>"></script>
<script src="<c:url value='/static/js/controller/book_controller.js'/>"></script>
<div class="generic-container" ng-controller="BookController as ctrl">
  <div class="panel panel-default">
    <!-- Default panel contents -->
    <div class="panel-heading"><span class="lead">List of Books </span></div>
    <div class="tablecontainer">
      <table class="table table-hover">
        <thead>
        <tr>
          <th>Isbn</th>
          <th>Title</th>
          <th>Author</th>
          <th>Genre</th>
          <th>Quantity</th>
          <th>Price</th>
          <th width="20%"></th>
        </tr>
        </thead>
        <tbody>
        <tr ng-repeat="b in ctrl.books">
          <td><span ng-bind="b.isbn"></span></td>
          <td><span ng-bind="b.title"></span></td>
          <td><span ng-bind="b.author"></span></td>
          <td><span ng-bind="b.genre"></span></td>
          <td><span ng-bind="b.quantity"></span></td>
          <td><span ng-bind="b.price"></span></td>
          <td>
            <button type="button" ng-click="ctrl.changeView()" class="btn btn-success custom-width">Edit</button>
            <button type="button" ng-click="ctrl.remove(b.isbn)" class="btn btn-danger custom-width">Remove</button>
          </td>
        </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

</body>
</html>
