<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Login page</title>
    <link href="<c:url value='/static/css/bootstrap.css' />"  rel="stylesheet"></link>
    <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.css" />
    <style>
        .error {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
        }

        .msg {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }
    </style>
</head>
<body>
<div class="container">
    <form class="form-horizontal" name="loginForm" action="login" method='POST'>
        <div class="form-group">
            <h2 class="col-md-offset-6">Welcome</h2>
        </div>
        <div class="form-group">
            <c:choose>
                <c:when test="${not empty error}">
                    <div class="error">${error}</div>
                </c:when>

                <c:when test="${not empty msg}">
                    <div class="msg">${msg}</div>
                </c:when>
            </c:choose>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="inputUsername">Username</label>
            <div class="col-md-10">
                <input class="form-control" type="text"
                       id="inputUsername" name="username" placeholder="Username">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-2 control-label" for="inputPassword">Password</label>
            <div class="col-md-10">
                <input class="form-control" type="password"
                       id="inputPassword" name="password" placeholder="Password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <button class="btn btn-default" type="submit">Login</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>